import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl = 'http://localhost:8000/';
  httpHeaders = new HttpHeaders({'Content-Type':'application/json'});
  constructor(private http: HttpClient) { }

  saveMember(member:any) : Observable<any>{
    let body =
      {
        name : member.name ,
        surname : member.surname,
        phone : member.phone
      }
    return this.http.post(this.baseUrl + 'members/', body,
    {headers:this.httpHeaders}
    );
  };
}
