import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'members-front';

  selected_member = {id: 0, name:'', surname:'', phone:''};

  members = [
    {
      id: 1,
      name: "Fulano",
      surname: "dos Anzois",
      phone: "95995595959",
      photo:"https://images.pexels.com/photos/2777898/pexels-photo-2777898.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
    },
    {
      id: 2,
      name: "Ciclano",
      surname: "dos Anzois",
      phone: "95995595959",
      photo:"https://images.pexels.com/photos/2777898/pexels-photo-2777898.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
    },
    {
      id: 3,
      name: "Beltrano",
      surname: "dos Anzois",
      phone: "95995595959",
      photo:"https://images.pexels.com/photos/2777898/pexels-photo-2777898.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
    }
  ]

  constructor(private api:ApiService, private router: Router){
    this.getMembers();
  }

  getMembers = ()=>{
    this.api.getAllMembers().subscribe(
      data => {
        this.members = data
      },
      error =>{
        console.log("Aconteceu erro", error);
      }
    );
  }

  memberClicked = (member:any) => {
   this.router.navigate(['member-detail', member.id])
  }
}
