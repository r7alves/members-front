import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from './api.service';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css']
})
export class MemberDetailComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
    private appComponent : AppComponent,
    private router: Router) { }

  selected_member =  {id: null, name:'', surname:'', phone:'', photo:''};
  selected_id : any;

  ngOnInit(): void {
    this.route.paramMap.subscribe((param:ParamMap) => {
     let id = parseInt(param.get('id') as any)
     this.selected_id = id
     this.loadMember(id)
    })
  }

  loadMember(id :any) {
    this.api.getMember(id).subscribe(
      data => {
        this.selected_member = data
      },
      error =>{
        console.log("Aconteceu erro", error);
      }
    );
  }
  update() {
    this.api.updateMember(this.selected_member).subscribe(
      data => {

        this.selected_member = data
      },
      error =>{
        console.log("Aconteceu erro", error);
      }
    );
  }
  delete() {
    this.api.deleteMember(this.selected_id).subscribe(
      data => {
        let index : any;
        this.appComponent.members.forEach((e,i) =>{
          if(e.id == this.selected_id)
            index = i;
        });

        this.appComponent.members.splice(index, 1)
      },
      error =>{
        console.log("Aconteceu erro", error);
      }
    );
  }

  newMember(){
    this.router.navigate(['new-member'])
  }

}
